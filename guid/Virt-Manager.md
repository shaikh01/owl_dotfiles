# Установка Virt-Manager на VoidLinux
```
sudo xbps-install virt-manager qemu bridge-utils dnsmasq

sudo usermod -aG kvm username
sudo usermod -aG libvirt username

sudo ln -s /etc/sv/libvirtd /var/service
sudo ln -s /etc/sv/virtlockd /var/service
sudo ln -s /etc/sv/virtlogd /var/service
sudo ln -s /etc/sv/dbus /var/service
```
  
# Установка Virt-Manager на ArchLinux
```
sudo pacman -S virt-manager qemu bridge-utils dnsmasq

sudo usermod -aG kvm username
sudo usermod -aG libvirt username

sudo systemctl enable libvirtd.service
```
  
# Установка Virt-Manager на AltLinux
```
sudo epmi virt-manager qemu bridge-utils dnsmasq libvirt

sudo gpasswd -a username vmusers

sudo systemctl enable libvirtd.service
```