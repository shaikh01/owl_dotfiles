## Установка AltLinux jeos-systemd

#### Установка epm:
```
apt-get update
apt-get install eepm
```
  
#### Создание пользователя
```
epmi sudo micro

useradd -m -g users -G wheel -s /bin/bash username
passwd username

micro /etc/sudoers:
    username ALL=(ALL) ALL
```
  
#### Установка XORG
```
sudo epmi xorg-server xinit xinitrc xorg-drv-video xorg-utils mesa-dri-drivers libEGL vulkan-amdgpu

!!Это не нужно ставить!! xkill glxgears xorg-xvfb appres glxinfo xdm xorg-drv-libinput xorg-drv-mga xorg-drv-wacom kernel-modules-drm-un-def 
```

#### Установка BSPWM
```
sudo empi bspwm sxhkd alacritty numlockx git

mkdir .config/bspwm && mkdir .config/sxhkd

micro .xinitrc :
exec sxhkd &
exec bspwm 

micro .config/bspwm/bspwmrc :

#! /bin/bash
sxhkd &
alacritty &

micro .config/sxhkd/sxhkdrc :
super + {_,shift + }Return
	{alacritty}

chmod u+x .config/bspwm/bspwmrc  
```

#### Установка Telegram
```
sudo epmi telegram-desktop fonts-ttf-open-sans at-spi2-core
```
  
#### Настройка раскладки клавиатуры для иксов
```
sudo micro /etc/X11/xorg.conf.d/00-keyboard.conf

Section "InputClass"
    Identifier "system-keyboard"
    MatchIsKeyboard "on"
    Option "XkbLayout" "us,ru"
    Option "XkbModel" "pc105" 
    Option "XkbOptions" "grp:alt_shift_toggle"
EndSection
```
  
#### Установка Fish
```
sudo epmi fish
fish
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
fisher install jorgebucaran/nvm.fish
fisher install IlanCosman/tide@v5
!!chsh -s /usr/bin/fish
set -U fish_greeting

tide configure - что бы конфигурировать тильды
```
  
#### Установка софта
```
sudo apt-get install pulseaudio pavucontrol inkscape ghostscript blender gimp viewnior firefox telegram-desktop
obs-studio nemo htop cmus neofetch scrot cava ranger neovim mpv unzip polybar jq fish rofi feh
```