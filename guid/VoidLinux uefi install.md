## Установка VoidLinux с uefi
У меня интернет в комп по кабелю заходит так что сеть при установке дистров я не настраиваю. И вообще нужно [официальную документацию](https://docs.voidlinux.org/installation/guides/chroot.html) смотреть, а не мои писульки.  
```  
Разметка диска

cfdisk /dev/sdd (make partition for root & efi)
mkfs.vfat /dev/sdd4
mkfs.btrfs -f /dev/sdd5
```
   
```
Монтирование дисков

mount /dev/sdd5 /mnt
mkdir -p /mnt/boot/efi/
mount /dev/sdd4 /mnt/boot/efi/
```
  
```
Выбор репозитория, архитектуры, копирование ключей

REPO=https://repo-default.voidlinux.org/current
ARCH=x86_64
mkdir -p /mnt/var/db/xbps/keys
cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys/
```
  
```
Установка системы

XBPS_ARCH=$ARCH xbps-install -S -r /mnt -R "$REPO" base-system
```
  
```
Это для DHCPCD нужно

cp /etc/resolv.conf /mnt/etc/
```
  
```
Chroot в установленную систему

xchroot /mnt /bin/bash
```
  
```
Установка софта

xbps-install micro grub-x86_64-efi os-prober dhcpcd
```
  
```
Настройка локалей и временной зоны

micro /etc/default/libc-locales (uncomment en_US.UTF-8 & ru_RU.UTF-8)
xbps-reconfigure -f glibc-locales

ln -sf /usr/share/zoneinfo/Asia/Novosibirsk /etc/localtime
```
  
```
Настройка user'ов

passwd
useradd -m -g users -G wheel -s /bin/bash username
usermod -aG audio username
passwd user
EDITOR=micro visudo user ALL=(ALL) ALL
```

```
Генерация fstab

cp /proc/mounts /etc/fstab
```

```
Установка grub

xbps-install grub-x86_64-efi
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="Void"
echo GRUB_DISABLE_OS_PROBER=false >> /etc/default/grub
update-grub
```
  
```
Реконфигурирование системы, выход и перезагрузка

xbps-reconfigure -fa
exit
umount -R /mnt
reboot
```
  
## Настройка после установки

```
Запуск DHCPCD

sudo ln -s /etc/sv/dhcpcd /var/service
sudo sv up dhcpcd
``` 

```
Установка alsa

sudo xbps-install alsa-lib alsa-lib-devel alsa-plugins alsa-tools alsa-utils
sudo ln -s /etc/sv/alsa /var/service
sudo sv up alsa
```
  
```
Установка chrony

sudo xbps-install chrony
sudo ln -s /etc/sv/chronyd /var/service
sudo sv up chronyd
```
  
```
Настройка раскладки клавиатуры для иксов

sudo micro /etc/X11/xorg.conf.d/00-keyboard.conf
  
Section "InputClass"
    Identifier "system-keyboard"
    MatchIsKeyboard "on"
    Option "XkbLayout" "us,ru"
    Option "XkbModel" "pc105" 
    Option "XkbOptions" "grp:alt_shift_toggle"
EndSection
```
  
```
Установка fish

sudo xbps-install fish-shell
fish
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
fisher install jorgebucaran/nvm.fish
fisher install IlanCosman/tide@v5
chsh -s /usr/bin/fish
set -U fish_greeting

tide configure - что бы конфигурировать тильды
```