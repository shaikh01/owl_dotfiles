# Автостарт и Автологин в оконных менеджерах.
Привет, тут способы запуска оконных менеджеров на xorg и на wayland без логин-менеджеров и дополнительного софта. Сразу предупреждаю при очередном обновлении системы все может крашнутся, нужно будет заново все сделать, секунд 30-40 не более. Все тестировалось на ArchLinux и на VoidLinux ну и когда то на Ubuntu, Debian и Devuan.
  
И так же хочу тебя предупредить, если ты много эксперементируешь то лучше отказаться от автостарта графического сервера, в случае проблем система будет грузить мертвые иксы и только chroot в помощь)

Принцип работы: запуск инита автологина, а после этого запуск того что в .xinitrc прописанно. И на всякий случай сверяйся с оффициальной документацией твоего дистрибутива, что то может поменяться, а где то я мог косячнуть.
  
  
## | Systemd | Bash | Xorg | WM |
  
```
### Автостарт

В ~/.bash_profile нужно добавить:

if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi
```
  
```
### Автологин

В /etc/systemd/system/getty@tty1.service.d/override.conf нужно добавить:

[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --autologin USERNAME --noclear %I $TERM

Не забудь USERNAME заменить на свой логин
```
  
  
## | Systemd | Fish | Xorg | WM |
  
```
### Автостарт

В ./config/fish/config.fish нужно добавить:

if status is-login
    if test -z "$DISPLAY" -a "$(tty)" = /dev/tty1
        exec startx -- -keeptty
    end
end
```
  
```
### Автологин

В /etc/systemd/system/getty@tty1.service.d/override.conf нужно добавить:

[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --autologin USERNAME --noclear %I $TERM

Не забудь USERNAME заменить на свой логин
```

## | Runit | Bash | Xorg | WM |
  
```
### Автостарт

В ~/.bash_profile нужно добавить:

if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi
```
  
```
### Автологин

/etc/sv/agetty-tty1/conf нужно привести к виду:

if [ -x /sbin/agetty -o -x /bin/agetty ]; then
	# util-linux specific settings
	if [ "${tty}" = "tty1" ]; then
		GETTY_ARGS="-a USERNAME --noclear"
	fi
fi

Не забудь USERNAME заменить на свой логин
```
  
## | Runit | Fish | Xorg | WM |
  
```
### Автостарт

В ./config/fish/config.fish нужно добавить:

if status is-login
    if test -z "$DISPLAY" -a "$(tty)" = /dev/tty1
        exec startx -- -keeptty
    end
end
```
  
```
### Автологин

/etc/sv/agetty-tty1/conf нужно привести к виду:

if [ -x /sbin/agetty -o -x /bin/agetty ]; then
	# util-linux specific settings
	if [ "${tty}" = "tty1" ]; then
		GETTY_ARGS="-a USERNAME --noclear"
	fi
fi

Не забудь USERNAME заменить на свой логин
```
  
## | Systemd | Bash | Wayland | WM |
  
```
### Автостарт на примене sway

В ~/.bash_profile нужно добавить:

if [ "$(tty)" = "/dev/tty1" ]; then
 exec sway
fi
```
  
```
### Автологин

В /etc/systemd/system/getty@tty1.service.d/override.conf нужно добавить:

[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --autologin USERNAME --noclear %I $TERM

Не забудь USERNAME заменить на свой логин
```