## Установка ArchLinux с uefi
У меня интернет в комп по кабелю заходит так что сеть при установке дистров я не настраиваю. И вообще нужно [официальную документацию](https://wiki.archlinux.org/title/Installation_guide) смотреть, а не мои писульки.  
  
```
Разметка диска

cfdisk /dev/sdaX (make partition for root & efi)
mkfs.fat -F 32 /dev/sdaX1
mkfs.ext4 /dev/sdaX2
```
  
```
Монтирование дисков

mount /dev/sdaX2 /mnt
mkdir /mnt/boot
mount /dev/sdaX1 /mnt/boot
```
  
```
Установка системы

pacstrap /mnt base linux linux-firmware
```
  
```
Генерация fstab

genfstab -U /mnt >> /mnt/etc/fstab
```
  
```
Chroot в установленную систему

arch-chroot /mnt
```
  
```
Установка софта

pacman -S grub efibootmgr micro sudo dhcpcd os-prober ntfs-3g
systemctl enable dhcpcd
```
  
```
Настройка локалей и временной зоны

micro /etc/locale.gen (uncomment en_US.UTF-8 & ru_RU.UTF-8)
locale-gen

ln -sf /usr/share/zoneinfo/Asia/Novosibirsk /etc/localtime
hwclock --systohc
```
  
```
Настройка user'ов

passwd
useradd -m -g users -G wheel -s /bin/bash username
passwd username
EDITOR=micro visudo username ALL=(ALL) ALL
```

```
Установка grub

mkdir /boot/efi
mount /dev/sdaX1 /boot/efi
grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/EFI --removable
grub-mkconfig -o /boot/grub/grub.cfg
```
  
```
Выход и перезагрузка

exit
umount -R /mnt
reboot
```
  
## Настройка после установки
  
Эти настройки я делаю для оконных менеджеров, для ДЕ есть свои инструменты.
  
```
Запуск DHCPCD

sudo systemctl enable dhcpcd
``` 

```
Установка alsa

sudo pacman -S alsa-lib alsa-lib-devel alsa-plugins alsa-tools alsa-utils
sudo systemctl enable alsa
```
  
```
Установка ntp

sudo pacman -S ntp
sudo systemctl enable ntpd
```
  
```
Настройка раскладки клавиатуры для иксов

sudo micro /etc/X11/xorg.conf.d/00-keyboard.conf

Section "InputClass"
    Identifier "system-keyboard"
    MatchIsKeyboard "on"
    Option "XkbLayout" "us,ru"
    Option "XkbModel" "pc105" 
    Option "XkbOptions" "grp:alt_shift_toggle"
EndSection
```
  
```
Установка fish

sudo pacman -S fish
fish
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
fisher install jorgebucaran/nvm.fish
fisher install IlanCosman/tide@v5
chsh -s /usr/bin/fish
set -U fish_greeting

tide configure - что бы конфигурировать тильды
```
  
```
Установка YAY

sudo pacman -S git base-devel
git clone https://aur.archlinux.org/yay.git  
cd yay  
makepkg -si  
```  