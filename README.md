# Привет!
  
Это репозиторий с дотами оконных менеджеров и окружений которые я делал для ютуб канала [**proLinux**](https://www.youtube.com/channel/UCYvMQ4fBOX2kwfJ981cZSPg). Если есть вопросы то их лучше всего в [телеграм](https://t.me/proLinux_tg) задавать, так я их сразу увижу и постараюсь ответить.
  
  
▪️ [**Дефолные конфиги оконных менеджеров и программ с переводом**](https://notabug.org/owl410/owl_dotfiles/src/master/soft)  
  
▪️ [**Мануалы**](https://notabug.org/owl410/owl_dotfiles/src/master/guid)  
  
▪️ [**Обои**](https://unsplash.com/@owl410/collections)  

# Галерея
[<img src=https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/sway/swayfx_gawr_gura/.img/photo_2023-08-12_18-03-12.jpg width="250" align="center">](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/sway/swayfx_gawr_gura) [<img src=https://notabug.org/owl410/owl_dotfiles/raw/master/Qtile/Qtile_1/.img/2023-01-02-030918_1920x1080_scrot.png width="250" align="center">](https://notabug.org/owl410/owl_dotfiles/src/master/Qtile/Qtile_1) [<img src=https://notabug.org/owl410/owl_dotfiles/raw/master/openbox/openbox_star_wars/img/2022-05-02-174235_1920x1080_scrot.png width="250" align="center">](https://notabug.org/owl410/owl_dotfiles/src/master/openbox/openbox_star_wars) [<img src=https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/hyprland/hypr_deep_blue/.img/photo_2023-08-17_15-19-16.jpg width="250" align="center">](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/hyprland/hypr_deep_bluee) [<img src=https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/sway/sway_fedora/.img/photo_2023-08-18_20-20-20.jpg width="250" align="center">](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/sway/sway_fedora) [<img src=https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/xfce/xfce_tokio_night/img/src_2.jpg width="250" align="center">](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/xfce/xfce_tokio_night)

# Dotfiles
* [Awesome Wm](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/awesome)
* [bspwm](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/bspwm)
* dwm
* [Hyprland](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/hyprland)
* [I3](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/i3wm)
* LabWc
* [NEWM](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/NEWM)
* Openbox
* Qtile
* [Sway](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/sway)
* [XFCE](https://notabug.org/owl410/owl_dotfiles/src/master/dotfiles/xfce/xfce_tokio_night)
  

# Другие ресурсы
| [Telegram](https://t.me/+VwBL8vNno-BkEyKK) |[Youtube](https://www.youtube.com/channel/UCYvMQ4fBOX2kwfJ981cZSPg) | [Discord](https://discord.gg/9hkUN8gw) | [Unsplash](https://unsplash.com/@owl410/collections) |
  
[Поддержать автора](https://www.donationalerts.com/r/prolinux)