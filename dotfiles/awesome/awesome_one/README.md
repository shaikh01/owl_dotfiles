## Gallery
<img src="1" width="450" align="center">
<img src="1" width="450" align="center">
<img src="1" width="450" align="center">
<img src="1" width="450" align="center">
  
## Info
|DIstro|[ARCH](https://archlinux.org/)|
|:---:|:---:|
|WM|[AwesomeWM-git](https://awesomewm.org/)|
|Bar|[Wibar](https://awesomewm.org/apidoc/popups_and_bars/awful.wibar.html)|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[--](--)|
|GTK3|[--](--)|
|Cursor|[--](--)|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
|Picom|[--)|
|Launcher|--|
  
## Настройка системы
[```Установка ArchLinux```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/ArchLinux%20uefi%20install.md)  
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  

  
## Install Awesome
```
sudo pacman -S xorg xorg-xinit mesa git base-devel alacritty base-devel  
yay awesome-git  
  
.xinitrc: exec awesome  
mkdir -p ~/.config/awesome/  
cp /etc/xdg/awesome/rc.lua ~/.config/awesome/
cp -r /usr/share/awesome/themes/default ~/.config/awesome/themes/default
``` 

## Soft 
```
sudo pacman -S pulseaudio pavucontrol pipewire lib32-pipewire wireplumber firefox telegram-desktop discord inkscape blender viewnior  
obs-studio nemo gedit htop links cmus neofetch scrot ranger  
lxappearance transmission-gtk  
``` 
  
## Picom
```
!!!yay picom-jonaburg-git  
```  
  
## Links:
https://www.reddit.com/r/unixporn/comments/op73tw/awesomewm_neumorphic_awesome/  
https://awesomewm.org/apidoc/popups_and_bars/awful.wibar.html  
https://github.com/saimoomedits/dotfiles  
