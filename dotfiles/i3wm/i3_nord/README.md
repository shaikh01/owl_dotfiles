## Галерея
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_nord/img/photo_2023-07-12_02-56-26.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_nord/img/photo_2023-07-12_02-56-37.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_nord/img/photo_2023-07-12_02-56-35.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_nord/img/photo_2023-07-12_02-56-31.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_nord/img/photo_2023-07-12_02-56-28.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_nord/img/photo_2023-07-12_02-56-33.jpg" width="350" align="center">

## Информация
|DIstro|[ARCH](https://archlinux.org/)|
|:---:|:---:|
|WM|[i3wm](https://i3wm.org/)|
|Bar|[Polybar](https://github.com/polybar/polybar)|
|Launcher|[Rofi](https://github.com/davatorium/rofi)|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[Nordzy-icon](https://github.com/alvatip/Nordzy-icon)|
|GTK3|[Nordic](https://github.com/EliverLara/Nordic)|
|Cursor|[Nordzy-cursors](https://github.com/alvatip/Nordzy-cursors)|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
  

## Темы для софта
[Alacritty_theme](https://github.com/nordtheme/alacritty), [Gedit_theme](https://github.com/nordtheme/gedit), [Blender_theme](https://github.com/TehMerow/Blender_Nord_Dark_Theme), [Telegram_theme](https://github.com/gilbertw1/telegram-nord-theme), [Micro_theme](https://github.com/KiranWells/micro-nord-tc-colors)
  
## ВАЖНО!!!
Я делал данный райс на Arch-Linux, все настройки идут от `~/.config/i3/themes/i3_nord/config` В первую очередь необходимо открыть этот файл и настроить все под себя.
  
Конфиги alacritty dunst picom polybar rofi и обои берутся из `~/.config/i3/themes/i3_nord/` 
  
## Настройка системы

[```Установка ArchLinux```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/ArchLinux%20uefi%20install.md)  
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  

## Установка I3wm
  
Тут самый минимум и главное не забудь шрифты, а то иконок не будет.
  
```
sudo pacman -S xorg xorg-xinit mesa i3-wm alacritty
  
.xinitrc: exec i3  
  
mkdir -p ~/.config/i3/  
cp /etc/i3/config ~/.config/i3/config 
```
  
## Установка софта
```
sudo pacman -S pulseaudio pavucontrol firefox inkscape blender telegram-desktop viewnior
obs-studio nemo htop links cmus neofetch scrot ranger neovim mpv unzip polybar fish picom rofi
conky ttf-nerd-fonts-symbols feh jq ueberzug w3m imagemagick ghostscript dunst
lxappearance nitrogen numlockx ttf-jetbrains-mono-nerd

yay cava 
``` 

## Установка темы i3_one
 
```
Склонировать репозиторий командой(предварительно нужно поставить пакет git): 

git clone https://notabug.org/owl410/owl_dotfiles
```  
  
```
Из ~/owl_dotfiles/dotfiles/i3wm/i3_nord/.config/ скопировать все в ~/.config можно мышкой в файловом менеджере.
 
cp -r ~/owl_dotfiles/dotfiles/i3wm/i3_nord/.config/ ~/.config
```  
  
```
Сделать исполняемыми все скрипты в ~/.config/i3/themes/i3_nord/scripts/:

sudo chmod -R u+x .config/i3/themes/i3_nord/scripts
```  
  
```
В ~/.xinitrc прописать:

exec i3 -c ~/.config/i3/themes/i3_nord/config
```  
  
```
Установить темы и обои при помощи lxappearance и nitrogen или как тебе удобно. Перезагрузится.

Темы для софта лежат в ~/owl_dotfiles/dotfiles/i3wm/i3_nord/soft_themes
```
  
