## Галерея
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_Star_Wars/img/photo_2023-07-07_02-16-30.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_Star_Wars/img/photo_2023-07-07_02-16-34.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_Star_Wars/img/photo_2023-07-07_02-16-38.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_Star_Wars/img/photo_2023-07-07_02-16-36.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_Star_Wars/img/photo_2023-07-07_02-16-28.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/i3wm/i3_Star_Wars/img/photo_2023-07-07_02-38-34.jpg" width="350" align="center">

## Информация
|DIstro|[ARCH](https://archlinux.org/)|
|:---:|:---:|
|WM|[i3wm](https://i3wm.org/)|
|Bar|[Polybar](https://github.com/polybar/polybar)|
|Launcher|[Rofi](https://github.com/davatorium/rofi)|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[Flat-Remix-GTK-Gray](https://github.com/daniruiz/Flat-Remix)|
|GTK3|[Flat-Remix-GTK-Gray](https://github.com/daniruiz/flat-remix-gtk)|
|Cursor|[Capitaine Cursors](https://github.com/keeferrourke/capitaine-cursors)|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
  
## ВАЖНО!!!
Я делал данный райс на Arch-Linux, все настройки идут от `~/.config/i3/themes/star_wars/config` В первую очередь необходимо открыть этот файл и настроить все под себя.
  
Конфиги alacritty dunst picom polybar rofi и обои берутся из `~/.config/i3/themes/star_wars/` 
  
## Настройка системы

[```Установка ArchLinux```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/ArchLinux%20uefi%20install.md)  
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  

## Установка I3wm
  
Тут самый минимум и главное не забудь шрифты, а то иконок не будет.
  
```
sudo pacman -S xorg xorg-xinit mesa i3-wm alacritty

.xinitrc: exec i3  
  
mkdir -p ~/.config/i3/  
cp /etc/i3/config ~/.config/i3/config 
```
  
## Установка софта
```
sudo pacman -S pulseaudio pavucontrol firefox inkscape blender telegram-desktop viewnior
obs-studio nemo htop links cmus neofetch scrot ranger neovim mpv unzip polybar fish picom rofi
conky ttf-nerd-fonts-symbols feh jq ueberzug w3m imagemagick ghostscript dunst
lxappearance nitrogen numlockx ttf-jetbrains-mono-nerd

yay cava  
``` 
  
## Установка темы Star_Wars
 
```
Склонировать репозиторий командой(предварительно нужно поставить пакет git): 

git clone https://notabug.org/owl410/owl_dotfiles
```  
  
```
Из ~/owl_dotfiles/dotfiles/i3wm/i3_star_wars/.config/ скопировать все в ~/.config можно мышкой в файловом менеджере.
 
cp -r ~/owl_dotfiles/dotfiles/i3wm/i3_star_wars/.config/ ~/.config
```  
  
```
Сделать исполняемыми все скрипты в ~/.config/i3/themes/star_wars/scripts/:

sudo chmod -R u+x .config/i3/themes/star_wars/scripts
```  
  
```
В ~/.xinitrc прописать:

exec i3 -c ~/.config/i3/themes/star_wars/config
```  
  
```
Установить темы и обои при помощи lxappearance и nitrogen или как тебе удобно. Перезагрузится.

Темы для софта лежат в ~/owl_dotfiles/dotfiles/i3wm/i3_star_wars/soft_themes
```
  
