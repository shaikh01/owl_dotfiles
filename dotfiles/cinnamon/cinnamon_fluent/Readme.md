## Галерея
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/cinnamon/cinnamon_fluent/img/photo_2023-10-03_23-18-12.jpg" width="450" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/cinnamon/cinnamon_fluent/img/photo_2023-10-03_23-18-17.jpg" width="450" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/cinnamon/cinnamon_fluent/img/photo_2023-10-03_23-18-21.jpg" width="450" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/cinnamon/cinnamon_fluent/img/photo_2023-10-03_23-18-24.jpg" width="450" align="center">

## Информация
|DIstro|[ARCH](https://archlinux.org/)|
|:---:|:---:|
|DE|[Cinnamon]|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[Delight-orange](https://www.pling.com/p/1532276)|
|GTK3|[Fluent](https://www.pling.com/p/1477941)|
|Cursor|[Default]|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|


## Настройка системы
```
sudo pacman -S xorg xorg-xinit cinnamon alacritty

micro .xinitrc:
exec cinnamon-session
```

## Установка софта
```
sudo pacman -S pulseaudio pavucontrol firefox inkscape blender telegram-desktop viewnior
obs-studio gnome-screenshot mousepad htop cmus neofetch neovim mpv file-roller conky ttf-nerd-fonts-symbols jq ttf-jetbrains-mono-nerd
``` 

## Настройка системы

[```Установка ArchLinux```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/ArchLinux%20uefi%20install.md)  
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  