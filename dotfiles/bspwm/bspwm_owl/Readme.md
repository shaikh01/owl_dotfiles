## Галерея
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_owl/.img/photo_2023-08-09_18-41-58.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_owl/.img/photo_2023-08-09_18-42-00.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_owl/.img/photo_2023-08-09_18-42-02.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_owl/.img/photo_2023-08-09_18-42-04.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_owl/.img/photo_2023-08-09_18-42-06.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_owl/.img/photo_2023-08-09_18-44-44.jpg" width="350" align="center">

## Информация
|DIstro|[ARCH](https://archlinux.org/)|
|:---:|:---:|
|WM|[BSPWM](https://github.com/baskerville/bspwm)|
|Bar|[Polybar](https://github.com/polybar/polybar)|
|Launcher|[rofi](https://github.com/davatorium/rofi)|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[La-Capitaine](https://github.com/keeferrourke/la-capitaine-icon-theme)|
|GTK3|[X-Arc-Plus](https://www.gnome-look.org/p/1167049/)|
|Cursors|[Capitaine-cursors](https://github.com/keeferrourke/capitaine-cursors)|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
  
## ВАЖНО!!!
Я делал данный райс на VoidLinux и на ArchLinux, все настройки идут от `~/.config/bspwm/themes/bspwm_owl/bspwm/bspwmrc` В первую очередь необходимо открыть этот файл и закоментировать/раскоментировать то что тебе нужно.
  
Конфиги bspwm sxhkd alacritty dunst picom polybar rofi и обои берутся из `~/.config/bspwm/themes/bspwm_owl` 
  
## Настройка системы

[```Установка ArchLinux```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/ArchLinux%20uefi%20install.md)  
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  
  
## Установка BSPWM
```
sudo pacman -S bspwm sxhkd xorg xorg-xinit mesa numlockx alacritty git libX11 libxft base-devel
  
chmod u+x .config/bspwm/bspwmrc  

.xinitrc:
exec sxhkd &
exec bspwm 
```
  
## Установка софта
```
sudo pacman -S pulseaudio pavucontrol firefox inkscape blender telegram-desktop viewnior
obs-studio nemo htop links cmus neofetch scrot ranger neovim mpv unzip polybar fish picom rofi
conky ttf-nerd-fonts-symbols feh jq ueberzug w3m imagemagick ghostscript

yay cava
``` 
  
## Установка темы Bspwm_Owl
 
```
Склонировать репозиторий командой(предварительно нужно поставить пакет git): 

git clone https://notabug.org/owl410/owl_dotfiles
```  
  
```
Из ~/owl_dotfiles/bspwm/bspwm_owl/.config скопировать все в ~/.config можно мышкой в файловом менеджере.
 
cp -r ~/owl_dotfiles/bspwm/bspwm_owl/.config/ ~/.config
```  
  
```
Сделать исполняемыми все скрипты в ~/.config/bspwm/themes/bspwm_owl/scripts:

chmod -R u+x .config/bspwm/themes/bspwm_owl/scripts
```  
  
```
В ~/.xinitrc прописать:
exec sxhkd -c ~/.config/bspwm/themes/bspwm_owl/sxhkd/sxhkdrc &  
exec bspwm -c ~/.config/bspwm/themes/bspwm_owl/bspwm/bspwmrc

И сделать chmod u+x ~/.config/bspwm/themes/bspwm_owl/bspwm/bspwmrc
```  
  
```
Установить темы и обои при помощи lxappearance и nitrogen или как тебе удобно. Заменить софт в ~/.config/bspwm/themes/bspwm_owl/sxhkd/sxhkdrc на нужный тебе. Перезагрузится.
```