## Галерея
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_nord/.img/photo_2023-08-08_17-22-20.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_nord/.img/photo_2023-08-08_17-24-05.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_nord/.img/photo_2023-08-08_17-22-22.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_nord/.img/photo_2023-08-08_17-22-24.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_nord/.img/photo_2023-08-08_17-22-27.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/bspwm_nord/.img/photo_2023-08-08_17-27-01.jpg" width="350" align="center">

## Информация
|DIstro|[ARCH](https://archlinux.org/)|
|:---:|:---:|
|WM|[BSPWM](https://github.com/baskerville/bspwm)|
|Bar|[Polybar](https://github.com/polybar/polybar)|
|Launcher|[rofi](https://github.com/davatorium/rofi)|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[Nordzy-icon](https://github.com/alvatip/Nordzy-icon)|
|GTK3|[Nordic](https://github.com/EliverLara/Nordic)|
|Cursor|[Nordzy-cursors](https://github.com/alvatip/Nordzy-cursors)|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
  
## Темы для софта
[Alacritty_theme](https://github.com/nordtheme/alacritty), [Gedit_theme](https://github.com/nordtheme/gedit), [Blender_theme](https://github.com/TehMerow/Blender_Nord_Dark_Theme), [Telegram_theme](https://github.com/gilbertw1/telegram-nord-theme), [Micro_theme](https://github.com/KiranWells/micro-nord-tc-colors)
  
## ВАЖНО!!!
Я делал данный райс на ArchLinux, все настройки идут от `~/.config/bspwm/themes/bspwm_nord/bspwm/bspwmrc` В первую очередь необходимо открыть этот файл и закоментировать/раскоментировать то что тебе нужно.
  
Конфиги bspwm sxhkd alacritty dunst picom polybar rofi и обои берутся из `~/.config/bspwm/themes/bspwm_nord` 
  
## Настройка системы

[```Установка ArchLinux```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/ArchLinux%20uefi%20install.md)  
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  
  
## Установка BSPWM
```
sudo pacman -S bspwm sxhkd xorg xorg-xinit mesa numlockx alacritty git libX11 libxft base-devel
  
chmod u+x .config/bspwm/bspwmrc  

.xinitrc:
exec sxhkd &
exec bspwm 
```
  
## Установка софта
```
sudo pacman -S pulseaudio pavucontrol firefox inkscape blender telegram-desktop viewnior
obs-studio nemo htop links cmus neofetch scrot ranger neovim mpv unzip polybar fish picom rofi
conky ttf-nerd-fonts-symbols feh jq ueberzug w3m imagemagick ghostscript

yay cava
``` 
  
## Установка темы Bspwm_Nord
 
```
Склонировать репозиторий командой(предварительно нужно поставить пакет git): 

git clone https://notabug.org/owl410/owl_dotfiles
```  
  
```
Из ~/owl_dotfiles/bspwm/bspwm_nord/.config скопировать все в ~/.config можно мышкой в файловом менеджере.
 
cp -r ~/owl_dotfiles/bspwm/bspwm_nord/.config/ ~/.config
```  
  
```
Сделать исполняемыми все скрипты в ~/.config/bspwm/themes/bspwm_nord/scripts/:

chmod -R u+x .config/bspwm/themes/bspwm_nord/scripts/
```  
  
```
В ~/.xinitrc прописать:
exec sxhkd -c ~/.config/bspwm/themes/bspwm_nord/sxhkd/sxhkdrc &  
exec bspwm -c ~/.config/bspwm/themes/bspwm_nord/bspwm/bspwmrc

И сделать chmod u+x ~/.config/bspwm/themes/bspwm_nord/bspwm/bspwmrc
```  
  
```
Установить темы и обои при помощи lxappearance и nitrogen или как тебе удобно. Заменить софт в ~/.config/bspwm/themes/bspwm_nord/sxhkd/sxhkdrc на нужный тебе. Перезагрузится.
```