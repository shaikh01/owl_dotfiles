## Галерея
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/gawr_gura/.img/photo_2023-08-04_16-25-44.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/gawr_gura/.img/photo_2023-08-04_16-25-47.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/gawr_gura/.img/photo_2023-08-04_16-25-49.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/gawr_gura/.img/photo_2023-08-04_16-25-53.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/gawr_gura/.img/photo_2023-08-04_16-25-51.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/bspwm/gawr_gura/.img/photo_2023-08-04_16-29-15.jpg" width="350" align="center">

## Информация
|DIstro|[ARCH](https://archlinux.org/)|
|:---:|:---:|
|WM|[BSPWM](https://github.com/baskerville/bspwm)|
|Bar|[Polybar](https://github.com/polybar/polybar)|
|Launcher|[rofi](https://github.com/davatorium/rofi)|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[Zorin](https://github.com/ZorinOS/zorin-icon-themes)|
|GTK3|[Zorin-Mint-Light](https://www.pling.com/p/1769479)|
|Cursors|[Capitaine-cursors](https://github.com/keeferrourke/capitaine-cursors)|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
  
## ВАЖНО!!!
Я делал данный райс на ArchLinux, все настройки идут от `~/.config/bspwm/themes/gawr_gura/bspwm/bspwmrc` В первую очередь необходимо открыть этот файл и закоментировать/раскоментировать то что тебе нужно.
  
Конфиги bspwm sxhkd alacritty dunst picom polybar rofi и обои берутся из `~/.config/bspwm/themes/gawr_gura` 
## Настройка системы

[```Установка ArchLinux```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/ArchLinux%20uefi%20install.md)  
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  
  
  
## Установка BSPWM
```
sudo pacman -S bspwm sxhkd xorg xorg-xinit mesa numlockx alacritty git libX11 libxft base-devel
  
chmod u+x .config/bspwm/bspwmrc  

.xinitrc:
exec sxhkd &
exec bspwm 
```
  
## Установка софта
```
sudo pacman -S pulseaudio pavucontrol firefox inkscape blender telegram-desktop viewnior
obs-studio nemo htop links cmus neofetch scrot ranger neovim mpv unzip polybar fish picom rofi
conky ttf-nerd-fonts-symbols feh jq ueberzug w3m imagemagick ghostscript

yay cava
``` 
  
## Установка темы Gawr-Gura
```
Склонировать репозиторий командой(предварительно нужно поставить пакет git): 

git clone https://notabug.org/owl410/owl_dotfiles
```  
  
```
Из ~/owl_dotfiles/bspwm/gawr_gura/.config скопировать все в ~/.config можно мышкой в файловом менеджере.
 
cp -r ~/owl_dotfiles/bspwm/gawr_gura/.config/ ~/.config
```  
  
```
Сделать исполняемыми все скрипты в ~/.config/bspwm/themes/gawr_gura/scripts:
sudo chmod -R u+x .config/bspwm/themes/gawr_gura/scripts
```  
  
```
В ~/.xinitrc прописать:
exec sxhkd -c ~/.config/bspwm/themes/gawr_gura/sxhkd/sxhkdrc &  
exec bspwm -c ~/.config/bspwm/themes/gawr_gura/bspwm/bspwmrc

И сделать chmod u+x ~/.config/bspwm/themes/gawr_gura/bspwm/bspwmrc
```  
  
```
Установить темы и обои при помощи lxappearance и nitrogen или как тебе удобно. Заменить софт в ~/.config/bspwm/themes/gawr_gura/sxhkd/sxhkdrc на нужный тебе. Перезагрузится.

Темы для софта лежат в ~/owl_dotfiles/bspwm/gawr_gura/
```