## Галерея
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/sway/sway_fedora/.img/photo_2023-08-18_20-20-20.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/sway/sway_fedora/.img/photo_2023-08-18_20-20-24.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/sway/sway_fedora/.img/photo_2023-08-18_20-20-22.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/sway/sway_fedora/.img/photo_2023-08-18_20-20-27.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/sway/sway_fedora/.img/photo_2023-08-18_20-20-26.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/sway/sway_fedora/.img/photo_2023-08-18_20-22-49.jpg" width="350" align="center">
  
## Информация
|DIstro|[Fedora](https://getfedora.org/)|
|:---:|:---:|
|WM|[Sway](https://swaywm.org/)|
|Bar|[Waybar](https://github.com/Alexays/Waybar)|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[Fluent](https://github.com/vinceliuice/Fluent-icon-theme)|
|GTK3|[Flat-Remix-GTK](https://github.com/daniruiz/Flat-Remix-GTK)|
|Cursors|[Capitaine cursors](https://github.com/keeferrourke/capitaine-cursors)|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
|Menu|wofi|
  
## ВАЖНО!!!
Я делал данный райс на Fedora, все настройки идут от `~/.config/sway/themes/sway_fedora/sway` В первую очередь необходимо открыть этот файл и закоментировать/раскоментировать то что тебе нужно.
  
Конфиги и обои берутся из `~/.config/sway/themes/sway_fedora` 
  
## Настройка системы
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  
  
  
## Установка Sway
```
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm  
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm  
  
sudo dnf install sway waybar alacritty git wofi grim slurp  
  
start: sway  
  
export MOZ_ENABLE_WAYLAND=1  
export QT_QPA_PLATFORM=wayland
```
  
## Установка софта
```
sudo dnf install pulseaudio pavucontrol firefox telegram-desktop mousepad gimp inkscape  
blender ghostscript obs-studio xdg-desktop-portal-wlr transmission-gtk python  
imv mpv nemo waybar grim slurp swaybg swaylock mako tar jq wofi htop cmus neofetch ranger unzip cava
``` 
  
## Установка темы Sway_Fedora
```
Склонировать репозиторий командой(предварительно нужно поставить пакет git): 

git clone https://notabug.org/owl410/owl_dotfiles
```  
  
```
Из ~/owl_dotfiles/dotfiles/sway/sway_fedora/.config скопировать все в ~/.config
можно мышкой в файловом менеджере.
 
cp -r ~/owl_dotfiles/dotfiles/sway/sway_fedora/.config/ ~/.config
```  
  
```
Сделать исполняемыми все скрипты в ~/.config/sway/themes/sway_fedora/scripts:
sudo chmod -R u+x .config/sway/themes/sway_fedora/scripts
```  
  
```
Запустить данный sway можно командой sway -c .config/sway/themes/sway_fedora/sway
```