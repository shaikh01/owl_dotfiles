## Галерея
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/NEWM/newm_nord/.img/photo_2023-07-29_14-50-56.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/NEWM/newm_nord/.img/photo_2023-07-29_14-50-58.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/NEWM/newm_nord/.img/photo_2023-07-29_14-51-01.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/NEWM/newm_nord/.img/photo_2023-07-29_14-51-04.jpg" width="350" align="center">

## Информация
|DIstro|[ARCH](https://archlinux.org/)|
|:---:|:---:|
|WM|[NEWM](https://github.com/jbuchermn/newm)|
|Bar|[Waybar](https://github.com/Alexays/Waybar)|
|Launcher|[Wofi](https://hg.sr.ht/~scoopta/wofi)|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[Nordzy](https://github.com/alvatip/Nordzy-icon)|
|GTK3|[Nordic](https://github.com/EliverLara/Nordic)|
|Cursors|[Nordzy](https://github.com/alvatip/Nordzy-cursors)|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
  
## ВАЖНО!!!
Я делал данный райс на Arch-Linux, все настройки идут от `~/.config/newm/config.py` В первую очередь необходимо открыть этот файл и настроить все под себя. Проект Newm скорее мертв чем жив, есть конечно какие то форки, но оригинал вроде бы все.
  
Конфиги alacritty и обои берутся из `~/.config/newm/` 
  
## Настройка системы

[```Установка ArchLinux```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/ArchLinux%20uefi%20install.md)  
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  

## Установка NEWM
  
Тут самый минимум и главное не забудь шрифты, а то иконок не будет.
  
```
sudo pacman -S wayland python-pywayland python-pywlroots mesa wlroots polkit alacritty waybar ttf-nerd-fonts-symbols ttf-jetbrains-mono-nerd xdg-desktop-portal-wlr    

yay newm-git  

Запуск newm: start-newm 
```
  
## Установка софта
```
sudo pacman -S pulseaudio pavucontrol firefox inkscape blender telegram-desktop 
obs-studio nemo tumbler git base-devel htop links cmus neofetch ranger 
wofi imv mpv grim wf-recorder

yay wlogout-git
``` 
  
## Установка темы Nord
 
```
Склонировать репозиторий командой(предварительно нужно поставить пакет git): 

git clone https://notabug.org/owl410/owl_dotfiles
```  
  
```
Из ~/owl_dotfiles/dotfiles/NEWM/newm_nord/.config/ скопировать все в ~/.config можно мышкой в файловом менеджере.
 
cp -r ~/owl_dotfiles/dotfiles/NEWM/newm_nord/.config/ ~/.config
```  
  
```
Теперь можно все запустить командой start-newm
```