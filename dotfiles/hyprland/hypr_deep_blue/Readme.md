## Галерея
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/hyprland/hypr_deep_blue/.img/photo_2023-08-17_15-19-16.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/hyprland/hypr_deep_blue/.img/photo_2023-08-17_15-19-13.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/hyprland/hypr_deep_blue/.img/photo_2023-08-17_15-19-09.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/hyprland/hypr_deep_blue/.img/photo_2023-08-17_15-19-20.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/hyprland/hypr_deep_blue/.img/photo_2023-08-17_15-19-18.jpg" width="350" align="center">
<img src="https://notabug.org/owl410/owl_dotfiles/raw/master/dotfiles/hyprland/hypr_deep_blue/.img/photo_2023-08-17_15-19-22.jpg" width="350" align="center">

## Информация
|DIstro|[ARCH](https://archlinux.org/)|
|:---:|:---:|
|WM|[Hyprland](https://hyprland.org/)|
|Bar|[Waybar](https://github.com/Alexays/Waybar)|
|Terminal|[Alacritty](https://github.com/alacritty/alacritty)|
|Shell|[Fish](https://fishshell.com/)|
|Icon|[Fluent](https://github.com/vinceliuice/Fluent-icon-theme)|
|GTK3|[Flat-Remix-GTK](https://github.com/daniruiz/Flat-Remix-GTK)|
|Cursors|[Capitaine cursors](https://github.com/keeferrourke/capitaine-cursors)|
|Fonts|[JetBrainsMono](https://www.jetbrains.com/lp/mono/)|
|Launcher|[wofi](https://sr.ht/~scoopta/wofi/)|

  
## ВАЖНО!!!
Я делал данный райс на ArchLinux, все настройки идут от `~/.config/hypr/themes/hypr_deep_blue/hypr/config` В первую очередь необходимо открыть этот файл и закоментировать/раскоментировать то что тебе нужно.
  
Конфиги и обои берутся из `~/.config/hypr/themes/hypr_deep_blue` 
## Настройка системы

[```Установка ArchLinux```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/ArchLinux%20uefi%20install.md)  
[```Установка Apparmor```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Apparmor.md)  
[```Установка Lutris```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Lutris.md)  
[```Автостарт и Автологин```](https://notabug.org/owl410/owl_dotfiles/src/master/guid/Autostart_wm.md)  
  
  
## Установка Hyprland
```
sudo pacman -S base-devel gdb ninja gcc cmake libxcb xcb-proto xcb-util  
xcb-util-keysyms libxfixes libx11 libxcomposite xorg-xinput libxrender  
pixman wayland-protocols cairo pango seatd libxkbcommon xcb-util-wm  
xorg-xwayland cmake wlroots mesa git meson polkit 
  
git clone --recursive https://github.com/hyprwm/Hyprland  
cd Hyprland  
git submodule init  
git submodule update  
sudo make install  
  
cp Hyprland/example/hyprland.conf ~/.config/hypr/
  
Hyprland - для того что бы запустить

Так же можно из реп поставить sudo pacman -S hyprland или из аура
```
  
## Установка софта
```
sudo pacman -S pulseaudio pavucontrol firefox telegram-desktop mousepad gimp inkscape  
blender ghostscript obs-studio xdg-desktop-portal-wlr transmission-gtk python  
imv mpv nemo waybar grim slurp swaybg swaylock mako jq wofi htop cmus neofetch ranger unzip
ttf-nerd-fonts-symbols

yay cava
``` 
  
## Установка темы Hyprland_deep_blue
```
Склонировать репозиторий командой(предварительно нужно поставить пакет git): 

git clone https://notabug.org/owl410/owl_dotfiles
```  
  
```
Из ~/owl_dotfiles/dotfiles/hyprland/hyprland_deep_blue/.config скопировать все в ~/.config
можно мышкой в файловом менеджере.
 
cp -r ~/owl_dotfiles/dotfiles/hyprland/hyprland_deep_blue/.config/ ~/.config
```  
  
```
Сделать исполняемыми все скрипты в ~/.config/hypr/themes/hypr_deep_blue/scripts
sudo chmod -R u+x ~/.config/hypr/themes/hypr_deep_blue/scripts
```  
  
```
Установка тем, иконок и курсоров:
gsettings set org.gnome.desktop.interface icon-theme Fluent-dark  
gsettings set org.gnome.desktop.interface gtk-theme Flat-Remix-GTK-Blue-Dark  
gsettings set org.gnome.desktop.interface cursor-theme capitaine-cursors
```  
  
```
В ~/.config/hypr/hyprland.conf заинклюженна ссылка на конфиг,
потому что я не знаю как сделать по другому) Точнее знаю но мне проверять лень)
```